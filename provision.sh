#! /bin/bash

set -ex

target_dir=`cd $(dirname ${0}) && pwd`

yum -y install tar

groupadd mysql
useradd -g mysql -d /var/empty/ -s /sbin/nologin -M mysql

cd /usr/local/src
curl -L http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.28-linux-glibc2.5-x86_64.tar.gz -o mysql-5.6.28-linux-glibc2.5-x86_64.tar.gz
tar zxvf mysql-5.6.28-linux-glibc2.5-x86_64.tar.gz

ln -s /usr/local/src/mysql-5.6.28-linux-glibc2.5-x86_64 /usr/local/mysql

chown -R mysql:mysql /usr/local/src/mysql-5.6.28-linux-glibc2.5-x86_64

echo "/usr/local/mysql/lib/" > /etc/ld.so.conf.d/mysql.conf

echo "export PATH=\$PATH:/usr/local/mysql/bin/" > /etc/profile.d/mysql.sh

yum -y install libaio
yum -y install perl
cp ${target_dir}/configs/my.cnf /etc/my.cnf
cp ${target_dir}/configs/init.d /etc/rc.d/init.d/mysql
chkconfig --add mysql
mkdir /etc/mysql.conf.d
chmod 755 /etc/rc.d/init.d/mysql

/usr/local/mysql/scripts/mysql_install_db --user=mysql --basedir=/usr/local/mysql/ --datadir=/usr/local/mysql/var
